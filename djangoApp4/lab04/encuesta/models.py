from django.db import models

class Region(models.Model):
    nregion= models.CharField(max_length=200)

class Candidato(models.Model):
    fkregion = models.ForeignKey(Region, on_delete=models.CASCADE)
    ncandidato = models.CharField(max_length=200)
    nucandidato = models.CharField(max_length=200)
    pcandidato = models.CharField(max_length=200)

    votos = models. IntegerField(default=0)